package com.example.androidobjectanimatorexample;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class AndroidObjectAnimatorActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main_listview);
		final ListView listview = (ListView) findViewById(R.id.listview);
		String[] values = new String[] { "Example One", "Example Two",
				"Example Three", "Example Four", "Example Five" };

		final ArrayList<String> list = new ArrayList<String>();
		for (int i = 0; i < values.length; ++i) {
			list.add(values[i]);
		}
		final StableArrayAdapter adapter = new StableArrayAdapter(this,
				android.R.layout.simple_list_item_1, list);
		listview.setAdapter(adapter);

		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, final View view,
					int position, long id) {
				final String item = (String) parent.getItemAtPosition(position);

				System.out.println("view.getTag() " + view.getTag()
						+ " view.getId() : " + view.getId() + " position "
						+ position);

				Intent intent = null;

				switch (position) {
				case 0:
					intent = new Intent(AndroidObjectAnimatorActivity.this,
							ExampleOneActivity.class);
					startActivity(intent);
					break;
				case 1:
					intent = new Intent(AndroidObjectAnimatorActivity.this,
							ExampleTwoActivity.class);
					startActivity(intent);
					break;

				case 2:
					intent = new Intent(AndroidObjectAnimatorActivity.this,
							ExampleThreeActivity.class);
					startActivity(intent);
					break;
				case 3:
					intent = new Intent(AndroidObjectAnimatorActivity.this,
							ExampleFourActivity.class);
					startActivity(intent);
					break;
				case 4:
					intent = new Intent(AndroidObjectAnimatorActivity.this,
							ExampleFiveActivity.class);
					startActivity(intent);
					break;
				default:
					break;
				}

				/*
				 * view.animate().setDuration(2000).alpha(0) .withEndAction(new
				 * Runnable() {
				 * 
				 * @Override public void run() { list.remove(item);
				 * adapter.notifyDataSetChanged(); view.setAlpha(1); } });
				 */
			}

		});

	}

	private class StableArrayAdapter extends ArrayAdapter<String> {

		HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

		public StableArrayAdapter(Context context, int textViewResourceId,
				List<String> objects) {
			super(context, textViewResourceId, objects);
			for (int i = 0; i < objects.size(); ++i) {
				mIdMap.put(objects.get(i), i);
			}
		}

		@Override
		public long getItemId(int position) {
			String item = getItem(position);
			return mIdMap.get(item);
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

	}

}