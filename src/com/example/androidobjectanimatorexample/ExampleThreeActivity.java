package com.example.androidobjectanimatorexample;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ExampleThreeActivity extends Activity implements AnimationListener {

	TextView tv_menuOne;
	TextView tv_menuTwo;
	TextView tv_menuThree;
	Animation animation1;
	Animation animation2;
	Button rotate, translate;
	Context context = this;
	LinearLayout menuLayout;

	private int device_width;
	private int device_height;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.example_three_layout);
		tv_menuOne = (TextView) findViewById(R.id.menuOne);
		tv_menuTwo = (TextView) findViewById(R.id.menuTwo);
		tv_menuThree = (TextView) findViewById(R.id.menuThree);
		rotate = (Button) findViewById(R.id.rotate);
		translate = (Button) findViewById(R.id.translate);
		tv_menuOne.setBackgroundResource(R.drawable.shape);
		tv_menuTwo.setBackgroundResource(R.drawable.shape);

		// menuLayout = (LinearLayout) findViewById(R.id.menuLayout);

		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		device_width = size.x;
		device_height = size.y;
		float heightf = (float) device_height;
		float widthf = (float) device_width;
		tv_menuOne.setY(heightf / 2);
		tv_menuOne.setX(widthf / 2);

		tv_menuTwo.setY((heightf / 2) - 100);
		tv_menuTwo.setX((widthf / 2) - 100);

		tv_menuThree.setY((heightf / 2) - 200);
		tv_menuThree.setX((widthf / 2) - 200);

		animation1 = AnimationUtils.loadAnimation(this, R.anim.rotate);

		userInputHandler();

	}

	private void startAnimation() {
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		device_width = size.x;
		device_height = size.y;

		ValueAnimator animator = ValueAnimator.ofFloat(0, 2); // values from 0
																// to 1
		animator.setDuration(2000); // 5 seconds duration from 0 to 1
		animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {

				System.out.println("device_height " + device_height
						+ " ::: device_width " + device_width);
				float heightf = (float) device_height / 2;
				float widthf = (float) device_width / 2;

				System.out.println("heightf " + heightf + " ::: widthf "
						+ widthf);

				float value = ((Float) (animation.getAnimatedValue()))
						.floatValue();
				

				float menuOneX = (float) (300 * Math.sin(value * Math.PI));
				float menuOneY = (float) ((heightf / 2) * Math.cos(value * Math.PI));
				System.out.println("animcation Float value : " + value +" menuOneX "+menuOneX+" menuOneY "+menuOneY);

				tv_menuOne.setTranslationX(menuOneX);
				tv_menuOne.setTranslationY(menuOneY);

				tv_menuTwo.setTranslationX(menuOneX);
				tv_menuTwo.setTranslationY(menuOneY);

				tv_menuThree.setTranslationX(menuOneX);
				tv_menuThree.setTranslationY(menuOneY);

			}
		});

		animator.start();
	}

	private void userInputHandler() {

		rotate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				tv_menuOne.startAnimation(animation1);
			}
		});

		translate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startAnimation();
			}
		});

	}

	@Override
	public void onAnimationEnd(Animation animation) {
		tv_menuOne.setVisibility(View.INVISIBLE);

	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		tv_menuOne.setVisibility(View.VISIBLE);

	}

	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub
		tv_menuOne.setVisibility(View.VISIBLE);

	}
}