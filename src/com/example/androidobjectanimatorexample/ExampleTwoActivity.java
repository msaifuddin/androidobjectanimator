
package com.example.androidobjectanimatorexample;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ExampleTwoActivity extends Activity {

    private static final String TAG = "ExampleTwoActivity";

    /** Called when the activity is first created. */

    TextView tv_menuOne;

    TextView tv_menuTwo;

    TextView tv_menuThree;

    private int device_width;

    private int device_height;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.example_two_layout);

        tv_menuOne = (TextView)findViewById(R.id.menuOne);
        tv_menuTwo = (TextView)findViewById(R.id.menuTwo);
        tv_menuThree = (TextView)findViewById(R.id.menuThree);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        device_width = size.x;
        device_height = size.y;
        Log.d(TAG, device_width + " ::: " + device_height);
        float heightf = (float)device_height;

        tv_menuOne.setY(heightf);

        float heightf2 = (float)(device_height - 50);
        tv_menuTwo.setY(heightf2);

        float heightf3 = (float)(device_height - 100);
        tv_menuThree.setY(heightf3);

        loadMenu();

        Button menuButton = (Button)findViewById(R.id.menuButton);
        menuButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                loadMenu();
            }
        });

        tv_menuOne.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Toast.makeText(getApplicationContext(), "Animator Button Clicked",
                        Toast.LENGTH_SHORT).show();
            }
        });

        tv_menuTwo.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View arg0) {

            }
        });
    }

    private void loadMenu() {

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        device_width = size.x;
        device_height = size.y;
        Log.d(TAG, device_width + " ::: " + device_height);
        float heightf = (float)device_height;
        float widthf = (float)device_width - device_width;

        tv_menuOne.setY(heightf);
        tv_menuOne.setX(widthf);

        float heightf2 = (float)(device_height - 50);
        float widthf2 = (float)(device_width - device_width - 50);
        tv_menuTwo.setY(heightf2);
        tv_menuTwo.setX(widthf2);

        float heightf3 = (float)(device_height - 100);
        float widthf3 = (float)(device_width - device_width - 100);
        tv_menuThree.setY(heightf3);
        tv_menuThree.setX(widthf3);

        Log.d(TAG, "Menu One height " + heightf + " width " + widthf);
        Log.d(TAG, "Menu Two height " + heightf2 + " width " + widthf2);
        Log.d(TAG, "Menu Three height " + heightf3 + " width " + widthf3);

        float button_height = (device_height * 3) / 100;
        float button_width = (device_width * 10) / 100;
        Log.d(TAG, "button one x position " + button_width + "and y position " + button_height);
        ObjectAnimator animX = ObjectAnimator.ofFloat(tv_menuOne, "x", button_width);
        ObjectAnimator animY = ObjectAnimator.ofFloat(tv_menuOne, "y", button_height);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.setDuration(2000);
        animSetXY.playTogether(animX, animY);
        animSetXY.start();

        button_height = (device_height * 32) / 100;
        button_width = (device_width * 30) / 100;
        Log.d(TAG, "button Two  x position " + button_width + "and y position " + button_height);
        ObjectAnimator button2AnimX = ObjectAnimator.ofFloat(tv_menuTwo, "x", button_width);
        ObjectAnimator button2AnimY = ObjectAnimator.ofFloat(tv_menuTwo, "y", button_height);
        AnimatorSet button2animSetXY = new AnimatorSet();
        button2animSetXY.setDuration(2000);
        button2animSetXY.playTogether(button2AnimX, button2AnimY);
        button2animSetXY.start();

        button_height = (device_height * 60) / 100;
        button_width = (device_width * 10) / 100;
        Log.d(TAG, "button Three x position " + button_width + "and y position " + button_height);
        ObjectAnimator button3AnimX = ObjectAnimator.ofFloat(tv_menuThree, "x", button_width);
        ObjectAnimator button3AnimY = ObjectAnimator.ofFloat(tv_menuThree, "y", button_height);
        AnimatorSet button3animSetXY = new AnimatorSet();
        button3animSetXY.setDuration(2000);
        button3animSetXY.playTogether(button3AnimX, button3AnimY);
        button3animSetXY.start();
    }
}
